<?php

use \Cli\ProgressBar;

require '../vendor/autoload.php';

$progress = new ProgressBar(250);

for ($i = 0; $i <= 250; $i++) {
    $progress->update($i);
    usleep(1000000 / 4);
}
