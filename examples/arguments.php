<?php

use \Cli\Arguments;

require '../vendor/autoload.php';

$arguments = new Arguments();

$arguments->addArgument(
    'name',
    array(
        'help' => 'Name to greet',
        'default' => 'World',
    )
);

$arguments->addFlag(array('c', 'capitals'));

try {
    $options = $arguments->parse();
} catch (\Cli\ParsingException $e) {
    echo $e->getMessage();
}

$name = $options['name'];

if ($options['capitals']) {
    $name = strtoupper($name);
}

echo 'Hello, ' . $name . '!';
