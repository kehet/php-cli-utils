<?php

use \Cli\Color;

require '../vendor/autoload.php';

$textColors = array(
    'black',
    'darkGray',
    'red',
    'lightRed',
    'green',
    'lightGreen',
    'brown',
    'yellow',
    'blue',
    'lightBlue',
    'purple',
    'lightPurple',
    'cyan',
    'lightCyan',
    'lightGray',
    'white',
);

foreach ($textColors as $color) {
    echo Color::color($color, $color) . "\n";
}
