<?php

use \Cli\Table;

require '../vendor/autoload.php';

//
// example 1 using arrays
//

//*
$header = array(
    'Artist',
    'Country',
    'Sales',
);

$data = array(
    array(
        'Oomph!',
        'German',
        '10 551',
    ),
    array(
        'Eluveitie',
        'Switzerland',
        '21 767',
    ),
    array(
        'Deathstars',
        'Stockholm',
        '12 468',
    ),
    array(
        'Eisbrecher',
        'Germany',
        '2 860',
    ),
    array(
        'Edguy',
        'Germany',
        '961',
    ),
);

$options = array(
    'col2' => array(
        'align' => 'right',
        'format' => function($value) {
            return round($value, 2) . ' EUR';
        }
    ),
);

try {
    $table = new Table($header, $data, $options);
    $table->show();
} catch(\Exception $e) {
    echo $e->getMessage();
}
// */

//
// example 2 using PDO
//

//*
try {
    $table = new \Cli\Table();
    $db = new \PDO('mysql:host=localhost;dbname=test', 'H675X16em4', 'XOc1pu3AcAnINA2eKEY4ruRO8i8iXI');
    $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    $stmt = $db->prepare('SELECT * FROM `table`');
    $stmt->execute();

    $table->importFromPDO($stmt);
    $table->show();
} catch(\Exception $e) {
    echo $e->getMessage();
}
// */

//
// example 2 using mysqli
//

//*
try {
    $table = new \Cli\Table();
    $db = new \mysqli('localhost', 'H675X16em4', 'XOc1pu3AcAnINA2eKEY4ruRO8i8iXI', 'test');

    if ($db->connect_errno) {
        throw new \Exception("Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error);
    }

    if (!($stmt = $db->prepare("SELECT * FROM `table`"))) {
        throw new \Exception("Prepare failed: (" . $db->errno . ") " . $db->error);
    }

    if (!$stmt->execute()) {
        throw new \Exception("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
    }

    $table->importFromMysqli($stmt);
    $table->show();

    $stmt->close();
} catch (\Exception $e) {
    echo $e->getMessage();
    if (isset($db)) {
        $stmt->close();
    }
}
// */
