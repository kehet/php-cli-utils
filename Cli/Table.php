<?php

/**
 * Class for making nice tables
 *
 * @author Kehet
 */

namespace Cli;

class Table
{

    private $_header;
    private $_data;
    private $_options;
    private $_cellWidths;

    /**
     * @param null|array $header Array of headers
     * @param null|array $data Array of row arrays
     * @param null|array $options Options used on table (see setOptions doc)
     */
    public function __construct($header = array(), $data = array(), $options = array())
    {
        $this->setHeader($header);
        $this->setData($data);
        $this->setOptions($options);
        $this->_cellWidths = array();
    }

    /**
     * Set headers of table
     *
     * @param array $header Array of headers
     */
    public function setHeader($header = array())
    {
        $this->_header = $header;
    }

    /**
     * Set data to table and execute formatting automatically
     *
     * @param array $data Array of row arrays
     */
    public function setData($data = array())
    {
        $this->_data = $data;
        $this->_executeFormats();
    }

    /**
     * Set options to table or generate default values if nothing given as parameter
     *
     * examples:
     *
     * set first column to right aligned
     * $options['col0']['align'] = 'right';
     *
     * round 2nd column
     * $options['col1']['format'] = function($val) {
     *      return round($value);
     * }
     *
     * @param null|array $options
     */
    public function setOptions($options = array())
    {
        $this->_options = array();

        for ($i = 0; $i < count($this->_header); $i++) {
            $this->_options['col' . $i] = array_merge(
                array(
                    'align' => 'left',
                    'format' => null,
                ),
                (isset($options['col' . $i]) ? $options['col' . $i] : array())
            );
        }

        $this->_executeFormats();
    }

    /**
     * Import table headers and data from \PDOStatement
     *
     * @param \PDOStatement $stmt
     */
    public function importFromPDO(\PDOStatement $stmt)
    {
        $this->_header = array();
        $i = 0;
        while (($column = $stmt->getColumnMeta($i++)) !== false) {
            $this->_header[] = $column['name'];
        }

        $this->_data = array();
        while (($row = $stmt->fetch(\PDO::FETCH_NUM)) !== false) {
            $this->_data[] = $row;
        }

        // if options is empty we want to regenerate defaults
        if (count($this->_options) < 1) {
            $this->setOptions();
        }

        $this->_executeFormats();
    }

    /**
     * Import table headers and data from \mysqli_stmt
     *
     * @param \mysqli_stmt $stmt
     */
    public function importFromMysqli(\mysqli_stmt $stmt)
    {
        $meta = $stmt->result_metadata();
        $this->_header = array();
        while(($column = $field = $meta->fetch_field()) !== false)
        {
            $this->_header[] = $column->name;
        }

        $result = $stmt->get_result();
        $this->_data = array();
        while ($row = $result->fetch_array(MYSQLI_NUM))
        {
            $this->_data[] = $row;
        }

        // if options is empty we want to regenerate defaults
        if (count($this->_options) < 1) {
            $this->setOptions();
        }

        $this->_executeFormats();
    }

    /**
     * Applies formatting to all selected data
     */
    private function _executeFormats()
    {
        for ($i = 0; $i < count($this->_header); $i++) {
            if (isset($this->_options['col' . $i]['format']) AND is_callable($this->_options['col' . $i]['format'])) {
                foreach ($this->_data as &$row) {
                    $row[$i] = $this->_options['col' . $i]['format']($row[$i]);
                }
            }
        }
    }

    /**
     * Prints table
     */
    public function show()
    {
        echo $this->_getHorizontalLine() . "\n";

        echo '|';
        foreach ($this->_header as $key => $cell) {
            echo ' ' . sprintf('%-' . $this->_getLongestCellWidth($key) . 's', $cell) . ' |';
        }
        echo "\n";

        echo $this->_getHorizontalLine() . "\n";

        foreach ($this->_data as $row) {
            echo '|';
            foreach ($row as $key => $cell) {
                $align = '';
                if ($this->_options['col' . $key]['align'] == 'left') {
                    $align = '-';
                }

                echo ' ' . sprintf('%' . $align . $this->_getLongestCellWidth($key) . 's', $cell) . ' |';
            }
            echo "\n";
        }

        echo $this->_getHorizontalLine() . "\n";

    }

    /**
     * Return one horizontal line
     *
     * @return string
     */
    private function _getHorizontalLine()
    {
        $output = '+';

        foreach ($this->_data[0] as $key => $cell) {
            $output .= str_repeat('-', $this->_getLongestCellWidth($key) + 2) . '+';
        }

        return $output;
    }

    /**
     * Get current column width (also caches results for later use)
     *
     * @param $column Column to inspect
     *
     * @return int Width of column as characters
     */
    private function _getLongestCellWidth($column)
    {
        if (isset($this->_cellWidths[$column])) {
            return $this->_cellWidths[$column];
        }

        $max = -1;
        $_tmp = array_merge(array($this->_header), $this->_data);
        foreach ($_tmp as $row) {
            if (strlen($row[$column]) > $max) {
                $max = strlen($row[$column]);
            }
        }

        $this->_cellWidths[$column] = $max;
        return $max;
    }

}