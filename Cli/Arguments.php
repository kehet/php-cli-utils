<?php

/**
 * Class for argument parsing from CLI
 *
 * @author Kehet
 */

namespace Cli;

class Arguments
{

    private $_arguments;
    private $_flags;
    private $_parse;

    public function __construct()
    {
        $this->_arguments = array();
        $this->_flags = array();
        $this->_parse = array();
    }

    /**
     * Add new argument to except
     *
     * @param array|string $name Name of argument
     * @param string $options[help] Argument help text
     * @param boolean $options[required] If argument is mandatory or not
     * @param mixed $options[default] Default value of argument
     */
    public function addArgument($name, $options = array())
    {
        $options = array_merge(
            array(
                'help' => 'Value of ' . $name,
                'required' => false,
                'default' => null,
            ),
            $options
        );

        $this->_arguments[] = array_merge(array('name' => $name), $options);
    }

    /**
     * Add new flag to except
     *
     * @param array|string $names Name of flag
     * @param string $options[help] Flag help text
     * @param boolean $options[required] If flag is mandatory or not
     * @param mixed $options[default] Default value of flag (has no effect if $options[hasValue] = false)
     * @param boolean $options[hasValue] If flag contains value
     */
    public function addFlag($names, $options = array())
    {
        if (!is_array($names)) {
            $names = array($names);
        }

        $options = array_merge(
            array(
                'help' => null,
                'required' => false,
                'default' => false,
                'hasValue' => false,
            ),
            $options
        );

        $this->_flags[] = array_merge(array('names' => $names), $options);
    }

    /**
     * Parses arguments and returns results
     *
     * @param null|array $args Array of arguments, uses $_SERVER['argv'] if null
     *
     * @return array Array of parsed arguments and flags
     * @throws ParsingException
     */
    public function parse($args = null)
    {
        if ($args === null) {
            $args = array_slice($_SERVER['argv'], 1);
        }

        if (!is_array($args)) {
            if (strlen($args) == 0) {
                $args = array();
            } else {
                $args = explode(' ', $args);
            }
        }

        if (count($args) != 0) {
            foreach ($args as $arg) {
                // check if argument is flag
                if (substr($arg, 0, 1) == '-') {
                    // remove - and -- from beginning
                    $name = substr($arg, 1);
                    if (substr($name, 0, 1) == '-') {
                        $name = substr($name, 1);
                    }

                    // check if flag got value
                    $value = null;
                    $matches = array();
                    if (preg_match('/^(.+)=(.+)$/', $name, $matches) === 1) {
                        $name = $matches[1];
                        $value = $matches[2];

                        if (substr($value, 0, 1) == '"' AND substr($value, -1) == '"') {
                            $value = substr($value, 1, -1);
                        }
                    }

                    // try find flag
                    $found = false;
                    foreach ($this->_flags as $flag) {
                        if (in_array($name, $flag['names'])) {
                            if ($flag['hasValue'] == true) {
                                if ($value == null) {
                                    throw new ParsingException('Flag "' . $name . '" is undefined!');
                                }

                                foreach ($flag['names'] as $name) {
                                    $this->_parse[$name] = $value;
                                }
                            } else {
                                foreach ($flag['names'] as $name) {
                                    $this->_parse[$name] = true;
                                }
                            }
                            $found = true;
                            break;
                        }
                    }

                    // throw exception if flag was not found
                    if (!$found) {
                        throw new ParsingException('Flag "' . $name . '" is unknown!');
                    }
                } else {
                    $found = false;
                    foreach ($this->_arguments as $argument) {
                        if (!isset($this->_parse[$argument['name']])) {
                            $this->_parse[$argument['name']] = $arg;
                            $found = true;
                            break;
                        }
                    }

                    // throw exception if argument was not found
                    if (!$found) {
                        throw new ParsingException('Argument "' . $arg . '" is unknown!');
                    }
                }
            }
        }

        // check if all required flags and arguments was given
        foreach ($this->_arguments as $argument) {
            if (!isset($this->_parse[$argument['name']])) {
                if ($argument['required'] == true) {
                    throw new \Cli\ParsingException('Argument "' . $argument['name'] . '" was required!');
                }

                $this->_parse[$argument['name']] = $argument['default'];
            }
        }

        foreach ($this->_flags as $flag) {
            if (!isset($this->_parse[$flag['names'][0]])) {
                if ($flag['required'] == true) {
                    throw new \Cli\ParsingException('Flag "' . $flag['names'][0] . '" was required!');
                }

                foreach ($flag['names'] as $name) {
                    $this->_parse[$name] = $flag['default'];
                }

            }
        }

        return $this->_parse;
    }
}