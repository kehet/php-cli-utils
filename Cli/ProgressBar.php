<?php

/**
 * Class for showing progressbar
 *
 * @author Kehet
 */

namespace Cli;

class ProgressBar
{

    const SMOOTHING_FACTOR = 0.005;

    private $_max;
    private $_maxLength;

    private $_times;
    private $_lastSpeed;
    private $_averageSpeed;

    /**
     * @param int $max Max value of bar
     * @param int $maxLength Max length of bar as characters
     */
    function __construct($max = 100, $maxLength = 100)
    {
        $this->_max = $max;
        $this->_maxLength = $maxLength;

        $this->_times = array();
        $this->_lastSpeed = -1;
        $this->_averageSpeed = -1;
    }

    /**
     * Updates bar by printing \r and new bar
     *
     * @param $progress Current value of progress between 0 and value given on constructor
     */
    public function update($progress)
    {
        $this->_times[] = microtime(true);

        $done = ($progress == 0 ? 0 : round(($progress / $this->_max) * $this->_maxLength));
        $left = round((1 - ($progress / $this->_max)) * $this->_maxLength);

        // current/max
        $output = ' ' . sprintf('%' . strlen($this->_max) . 's/%s', $progress, $this->_max) . ' ';

        // progressbar
        $output .= '[' . str_repeat('=', ($done != 0 ? $done - 1 : $done)) . ($done > 0 ? '>' : '') . str_repeat(
                '-',
                $left
            ) . ']';

        // percent
        $output .= ' ' . sprintf('%6.2f', round(($progress / $this->_max) * 100, 2)) . "%";

        // eta
        // using http://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
        // averageSpeed = SMOOTHING_FACTOR * lastSpeed + (1-SMOOTHING_FACTOR) * averageSpeed
        if (count($this->_times) > 2) {

            $lastSpeed = $this->_times[count($this->_times) - 1] - $this->_times[count($this->_times) - 2];

            if ($this->_averageSpeed == -1) {
                $this->_averageSpeed = $lastSpeed;
            }

            $this->_averageSpeed = (ProgressBar::SMOOTHING_FACTOR * $lastSpeed + (1 - ProgressBar::SMOOTHING_FACTOR) * $this->_averageSpeed);

            $output .= ' ETA ' . $this->_mtime2str(round($this->_averageSpeed * ($this->_max - $progress)));
            $output .= ' ' . date('H:i:s', time() + round($this->_averageSpeed * ($this->_max - $progress)));
        }

        echo "\r" . $output . '  ';
    }

    /**
     * Converts seconds to human readable form hh:mm:ss
     *
     * @param $time Seconds
     *
     * @return string Time formatted to hh:mm:ss
     */
    private function _mtime2str($time)
    {
        return sprintf('%02d:%02d:%02d', floor($time / 3600), floor($time / 60) % 60, $time % 60);
    }
}