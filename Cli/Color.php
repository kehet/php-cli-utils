<?php

/**
 * Class for printing unix colored text
 *
 * @author Kehet
 */

namespace Cli;

class Color
{

    /**
     * @param $string String to color
     * @param null|string $textColor Foreground color (see function for colors)
     * @param null|string $bgColor Background color (see function for colors)
     *
     * @return string
     */
    public static function color($string, $textColor = null, $bgColor = null)
    {
        $textColors = array(
            'black' => '0;30',
            'darkGray' => '1;30',
            'red' => '0;31',
            'lightRed' => '1;31',
            'green' => '0;32',
            'lightGreen' => '1;32',
            'brown' => '0;33',
            'yellow' => '1;33',
            'blue' => '0;34',
            'lightBlue' => '1;34',
            'purple' => '0;35',
            'lightPurple' => '1;35',
            'cyan' => '0;36',
            'lightCyan' => '1;36',
            'lightGray' => '0;37',
            'white' => '1;37',
        );

        $bgColors = array(
            'black' => '40',
            'red' => '41',
            'green' => '42',
            'yellow' => '43',
            'blue' => '44',
            'magenta' => '45',
            'cyan' => '46',
            'lightGray' => '47',
        );

        $output = '';
        $coloring = false;

        $textColor = strtolower($textColor);
        $bgColor = strtolower($bgColor);

        if ($textColor != null AND isset($textColors[$textColor])) {
            $output .= "\033" . '[' . $textColors[$textColor] . 'm';
            $coloring = true;
        }

        if ($bgColor != null AND isset($bgColors[$bgColor])) {
            $output .= "\033" . '[' . $bgColors[$bgColor] . 'm';
            $coloring = true;
        }

        $output .= $string;

        if ($coloring == true) {
            $output .= "\033" . '[0m';
        }

        return $output;
    }

}