<?php

/**
 * Test suite for Arguments -parsing class
 *
 * @author Kehet
 */

class ArgumentsTests extends PHPUnit_Framework_TestCase
{

    public function testEmptyArgv1()
    {
        $arguments = new \Cli\Arguments();
        $arguments->parse("");
    }

    public function testEmptyArgv2()
    {
        $arguments = new \Cli\Arguments();
        $arguments->parse(array());
    }

    // arguments - required - positive tests

    public function testArgumentsNoneRequiredNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument('arg2');

        $output = $arguments->parse('');

        $this->assertNull($output['arg1']);
        $this->assertNull($output['arg2']);
    }

    public function testArgumentsSomeRequiredSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument(
            'arg1',
            array(
                'required' => true,
            )
        );
        $arguments->addArgument('arg2');

        $output = $arguments->parse('value1');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertNull($output['arg2']);
    }

    public function testArgumentsAllRequiredAllGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument(
            'arg1',
            array(
                'required' => true,
            )
        );
        $arguments->addArgument(
            'arg2',
            array(
                'required' => true,
            )
        );

        $output = $arguments->parse('value1 value2');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertEquals('value2', $output['arg2']);
    }

    // arguments - required - negative tests

    public function testArgumentsSomeRequiredNoneGiven()
    {
        $this->setExpectedException('\Cli\ParsingException');

        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument(
            'arg2',
            array(
                'required' => true,
            )
        );

        $arguments->parse('');
    }

    public function testArgumentsAllRequiredSomeGiven()
    {
        $this->setExpectedException('\Cli\ParsingException');

        $arguments = new \Cli\Arguments();
        $arguments->addArgument(
            'arg1',
            array(
                'required' => true,
            )
        );
        $arguments->addArgument(
            'arg2',
            array(
                'required' => true,
            )
        );

        $arguments->parse('value1');
    }

    // arguments - required - neutral tests

    public function testArgumentsNoneRequiredSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument('arg2');

        $output = $arguments->parse('value1');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertNull($output['arg2']);
    }

    public function testArgumentsSomeRequiredAllGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument(
            'arg1',
            array(
                'required' => true,
            )
        );
        $arguments->addArgument('arg2');

        $output = $arguments->parse('value1 value2');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertEquals('value2', $output['arg2']);
    }

    // arguments - default

    public function testArgumentsNoneDefaultsNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument('arg2');

        $output = $arguments->parse('');

        $this->assertNull($output['arg1']);
        $this->assertNull($output['arg2']);
    }

    public function testArgumentsSomeDefaultsNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument(
            'arg2',
            array(
                'default' => 'default2',
            )
        );

        $output = $arguments->parse('');

        $this->assertNull($output['arg1']);
        $this->assertEquals('default2', $output['arg2']);
    }

    public function testArgumentsAllDefaultsNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument(
            'arg1',
            array(
                'default' => 'default1',
            )
        );
        $arguments->addArgument(
            'arg2',
            array(
                'default' => 'default2',
            )
        );

        $output = $arguments->parse('');

        $this->assertEquals('default1', $output['arg1']);
        $this->assertEquals('default2', $output['arg2']);
    }

    public function testArgumentsNoneDefaultsSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument('arg2');

        $output = $arguments->parse('value1');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertNull($output['arg2']);
    }

    public function testArgumentsSomeDefaultsSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('arg1');
        $arguments->addArgument(
            'arg2',
            array(
                'default' => 'default2',
            )
        );

        $output = $arguments->parse('value1');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertEquals('default2', $output['arg2']);
    }

    public function testArgumentsAllDefaultsAllGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument(
            'arg1',
            array(
                'default' => 'default1',
            )
        );
        $arguments->addArgument(
            'arg2',
            array(
                'default' => 'default2',
            )
        );

        $output = $arguments->parse('value1 value2');

        $this->assertEquals('value1', $output['arg1']);
        $this->assertEquals('value2', $output['arg2']);
    }

    // flags - required - positive tests

    public function testFlagsNoneRequiredNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag('flag1');
        $arguments->addFlag('flag2');

        $output = $arguments->parse('');

        $this->assertFalse($output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsSomeRequiredSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'required' => true,
            )
        );
        $arguments->addFlag('flag2');

        $output = $arguments->parse('-flag1');

        $this->assertTrue($output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsAllRequiredAllGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'required' => true,
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'required' => true,
            )
        );

        $output = $arguments->parse('-flag1 -flag2');

        $this->assertTrue($output['flag1']);
        $this->assertTrue($output['flag2']);
    }

    // flags - required - negative tests

    public function testFlagsSomeRequiredNoneGiven()
    {
        $this->setExpectedException('\Cli\ParsingException');

        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'required' => true,
            )
        );
        $arguments->addFlag('flag2');

        $arguments->parse('');
    }

    public function testFlagsAllRequiredSomeGiven()
    {
        $this->setExpectedException('\Cli\ParsingException');

        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'required' => true,
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'required' => true,
            )
        );

        $arguments->parse('-flag1');
    }

    // flags - required - neutral tests

    public function testFlagsNoneRequiredSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag('flag1');
        $arguments->addFlag('flag2');

        $output = $arguments->parse('-flag1');

        $this->assertTrue($output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsSomeRequiredAllGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'required' => true,
            )
        );
        $arguments->addFlag('flag2');

        $output = $arguments->parse('-flag1 -flag2');

        $this->assertTrue($output['flag1']);
        $this->assertTrue($output['flag2']);
    }

    // flags - default

    public function testFlagsNoneDefaultsNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'hasValue' => true,
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'hasValue' => true,
            )
        );

        $output = $arguments->parse('');

        $this->assertFalse($output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsSomeDefaultsNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'hasValue' => true,
                'default' => 'default1',
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'hasValue' => true,
            )
        );

        $output = $arguments->parse('');

        $this->assertEquals('default1', $output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsAllDefaultsNoneGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'hasValue' => true,
                'default' => 'default1',
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'hasValue' => true,
                'default' => 'default2',
            )
        );

        $output = $arguments->parse('');

        $this->assertEquals('default1', $output['flag1']);
        $this->assertEquals('default2', $output['flag2']);
    }

    public function testFlagsNoneDefaultsSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'hasValue' => true,
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'hasValue' => true,
            )
        );

        $output = $arguments->parse('--flag1="value1"');

        $this->assertEquals('value1', $output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsSomeDefaultsSomeGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'hasValue' => true,
                'default' => 'default1',
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'hasValue' => true,
            )
        );

        $output = $arguments->parse('--flag1="value1"');

        $this->assertEquals('value1', $output['flag1']);
        $this->assertFalse($output['flag2']);
    }

    public function testFlagsSomeDefaultsAllGiven()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addFlag(
            'flag1',
            array(
                'hasValue' => true,
                'default' => 'default1',
            )
        );
        $arguments->addFlag(
            'flag2',
            array(
                'hasValue' => true,
            )
        );

        $output = $arguments->parse('--flag1="value1" --flag2="value2"');

        $this->assertEquals('value1', $output['flag1']);
        $this->assertEquals('value2', $output['flag2']);
    }

    public function testKitchenSink()
    {
        $arguments = new \Cli\Arguments();
        $arguments->addArgument('name');
        $arguments->addFlag(array('capital', 'c'));
        $arguments->addFlag(array('help', 'h'));
        $arguments->addFlag('verbose');

        $output = $arguments->parse('--help');

        $this->assertTrue($output['help']);
        $this->assertTrue($output['h']);
    }

}