<?php

/**
 *
 *
 * @author Kehet
 */

class TableTest extends PHPUnit_Framework_TestCase
{

    public function testBasicUsage()
    {
        $header = array(
            'Col 1',
            'Col 2',
            'Col 3',
        );

        $data = array(
            array(
                'Col 1 Row 1',
                'Col 2 Row 1',
                'Col 3 Row 1',
            ),
            array(
                'Col 1 Row 2',
                'Col 2 Row 2',
                'Col 3 Row 2',
            ),
            array(
                'Col 1 Row 3',
                'Col 2 Row 3',
                'Col 3 Row 3',
            ),
        );

        $table = new \Cli\Table($header, $data);

        $this->expectOutputString(
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1       | Col 2       | Col 3       |' . "\n" .
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1 Row 1 | Col 2 Row 1 | Col 3 Row 1 |' . "\n" .
            '| Col 1 Row 2 | Col 2 Row 2 | Col 3 Row 2 |' . "\n" .
            '| Col 1 Row 3 | Col 2 Row 3 | Col 3 Row 3 |' . "\n" .
            '+-------------+-------------+-------------+' . "\n"
        );

        $table->show();
    }

    public function testAlignment()
    {
        $header = array(
            'Col 1',
            'Col 2',
            'Col 3',
        );

        $data = array(
            array(
                'Col 1 Row 1',
                'Col 2 Row 1',
                '111',
            ),
            array(
                'Col 1 Row 2',
                'Col 2 Row 2',
                '2222',
            ),
            array(
                'Col 1 Row 3',
                'Col 2 Row 3',
                'Col 3 Row 3',
            ),
        );

        $options = array(
            'col2' => array(
                'align' => 'right',
            ),
        );

        $table = new \Cli\Table($header, $data, $options);

        $this->expectOutputString(
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1       | Col 2       | Col 3       |' . "\n" .
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1 Row 1 | Col 2 Row 1 |         111 |' . "\n" .
            '| Col 1 Row 2 | Col 2 Row 2 |        2222 |' . "\n" .
            '| Col 1 Row 3 | Col 2 Row 3 | Col 3 Row 3 |' . "\n" .
            '+-------------+-------------+-------------+' . "\n"
        );

        $table->show();

    }

    public function testFormat()
    {
        $header = array(
            'Col 1',
            'Col 2',
            'Col 3',
        );

        $data = array(
            array(
                'Col 1 Row 1',
                'Col 2 Row 1',
                'Col 3 Row 1',
            ),
            array(
                'Col 1 Row 2',
                'Col 2 Row 2',
                'Col 3 Row 2',
            ),
            array(
                'Col 1 Row 3',
                'Col 2 Row 3',
                'Col 3 Row 3',
            ),
        );

        $options = array(
            'col1' => array(
                'format' => function($value) {
                    return $value . '-suffix';
                },
            ),
        );

        $table = new \Cli\Table($header, $data, $options);

        $this->expectOutputString(
            '+-------------+--------------------+-------------+' . "\n" .
            '| Col 1       | Col 2              | Col 3       |' . "\n" .
            '+-------------+--------------------+-------------+' . "\n" .
            '| Col 1 Row 1 | Col 2 Row 1-suffix | Col 3 Row 1 |' . "\n" .
            '| Col 1 Row 2 | Col 2 Row 2-suffix | Col 3 Row 2 |' . "\n" .
            '| Col 1 Row 3 | Col 2 Row 3-suffix | Col 3 Row 3 |' . "\n" .
            '+-------------+--------------------+-------------+' . "\n"
        );

        $table->show();
    }

    public function testDatabaseDumpPDO()
    {
        $table = new \Cli\Table();
        $db = new \PDO('mysql:host=localhost;dbname=test', 'H675X16em4', 'XOc1pu3AcAnINA2eKEY4ruRO8i8iXI');

        $stmt = $db->prepare('SELECT * FROM `table`');
        $stmt->execute();

        $table->importFromPDO($stmt);

        $this->expectOutputString(
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1       | Col 2       | Col 3       |' . "\n" .
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1 Row 1 | Col 2 Row 1 | Col 3 Row 1 |' . "\n" .
            '| Col 1 Row 2 | Col 2 Row 2 | Col 3 Row 2 |' . "\n" .
            '| Col 1 Row 3 | Col 2 Row 3 | Col 3 Row 3 |' . "\n" .
            '+-------------+-------------+-------------+' . "\n"
        );

        $table->show();
    }

    public function testDatabaseDumpMysqli()
    {
        $table = new \Cli\Table();
        $db = new \mysqli('localhost', 'H675X16em4', 'XOc1pu3AcAnINA2eKEY4ruRO8i8iXI', 'test');

        if ($db->connect_errno) {
            throw new \Exception("Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error);
        }

        if (!($stmt = $db->prepare("SELECT * FROM `table`"))) {
            throw new \Exception("Prepare failed: (" . $db->errno . ") " . $db->error);
        }

        if (!$stmt->execute()) {
            throw new \Exception("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }

        $table->importFromMysqli($stmt);

        $this->expectOutputString(
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1       | Col 2       | Col 3       |' . "\n" .
            '+-------------+-------------+-------------+' . "\n" .
            '| Col 1 Row 1 | Col 2 Row 1 | Col 3 Row 1 |' . "\n" .
            '| Col 1 Row 2 | Col 2 Row 2 | Col 3 Row 2 |' . "\n" .
            '| Col 1 Row 3 | Col 2 Row 3 | Col 3 Row 3 |' . "\n" .
            '+-------------+-------------+-------------+' . "\n"
        );

        $table->show();

        $stmt->close();
    }

}
