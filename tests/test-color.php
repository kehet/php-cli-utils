<?php

/**
 * Test suite for Color class
 *
 * @author Kehet
 */

class ColorTest extends PHPUnit_Framework_TestCase
{

    public function testEmpty()
    {
        $this->assertEmpty(\Cli\Color::color(''));
    }

    public function testNoColoring()
    {
        $this->assertEquals('Test', \Cli\Color::color('Test'));
    }

    public function testColoring1()
    {
        $this->assertEquals("\033" . '[0;31mTest' . "\033" . '[0m', \Cli\Color::color('Test', 'red'));
    }

    public function testColoring2()
    {
        $this->assertEquals("\033" . '[0;36mTest' . "\033" . '[0m', \Cli\Color::color('Test', 'CyAn'));
    }

    public function testBackgroundColor1()
    {
        $this->assertEquals("\033" . '[41mTest' . "\033" . '[0m', \Cli\Color::color('Test', null, 'red'));
    }

    public function testBackgroundColor2()
    {
        $this->assertEquals("\033" . '[43mTest' . "\033" . '[0m', \Cli\Color::color('Test', null, 'yeLLow'));
    }

    public function testCombinedColors()
    {
        $this->assertEquals("\033" . '[0;31m' . "\033" . '[43mTest' . "\033" . '[0m', \Cli\Color::color('Test', 'red', 'yellow'));
    }

    public function testInvalidColor()
    {
        $this->assertEquals('Test', \Cli\Color::color('Test', 'NotAColor'));
    }

}
